namespace TerminalVentanilla.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SCE_TipoConsulta
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SCE_TipoConsulta()
        {
            SCE_Consultas = new HashSet<SCE_Consultas>();
        }

        [Key]
        [StringLength(10)]
        public string SCE_Id_TipoConsulta { get; set; }

        [Required]
        [StringLength(200)]
        public string Descripción { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SCE_Consultas> SCE_Consultas { get; set; }
    }
}
