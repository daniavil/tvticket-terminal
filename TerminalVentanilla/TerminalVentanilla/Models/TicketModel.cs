namespace TerminalVentanilla.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class TicketModel : DbContext
    {
        public TicketModel()
            : base("name=TicketModel")
        {
        }

        public virtual DbSet<SCE_Consultas> SCE_Consultas { get; set; }
        public virtual DbSet<SCE_DenunciaPorHurto> SCE_DenunciaPorHurto { get; set; }
        public virtual DbSet<SCE_Logs> SCE_Logs { get; set; }
        public virtual DbSet<SCE_Quiosko> SCE_Quiosko { get; set; }
        public virtual DbSet<SCE_Sugerencias> SCE_Sugerencias { get; set; }
        public virtual DbSet<SCE_Tipo_sugerencia> SCE_Tipo_sugerencia { get; set; }
        public virtual DbSet<SCE_TipoConsulta> SCE_TipoConsulta { get; set; }
        public virtual DbSet<TICKET_Atencion> TICKET_Atencion { get; set; }
        public virtual DbSet<TICKET_Mensaje> TICKET_Mensaje { get; set; }
        public virtual DbSet<TICKET_Sucursal> TICKET_Sucursal { get; set; }
        public virtual DbSet<TICKET_Ticket> TICKET_Ticket { get; set; }
        public virtual DbSet<TICKET_Ticket_Historico> TICKET_Ticket_Historico { get; set; }
        public virtual DbSet<TICKET_TokenApp> TICKET_TokenApp { get; set; }
        public virtual DbSet<TICKET_Ventanilla> TICKET_Ventanilla { get; set; }
        public virtual DbSet<TICKET_VentanillaAtencionSucursal> TICKET_VentanillaAtencionSucursal { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SCE_Quiosko>()
                .HasMany(e => e.SCE_Consultas)
                .WithRequired(e => e.SCE_Quiosko)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SCE_TipoConsulta>()
                .HasMany(e => e.SCE_Consultas)
                .WithRequired(e => e.SCE_TipoConsulta)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TICKET_Atencion>()
                .Property(e => e.CodTipo)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<TICKET_Atencion>()
                .Property(e => e.BtnMenu)
                .IsFixedLength();

            modelBuilder.Entity<TICKET_Atencion>()
                .HasMany(e => e.TICKET_Atencion1)
                .WithOptional(e => e.TICKET_Atencion2)
                .HasForeignKey(e => e.SegAtencion);

            modelBuilder.Entity<TICKET_Atencion>()
                .HasMany(e => e.TICKET_VentanillaAtencionSucursal)
                .WithRequired(e => e.TICKET_Atencion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TICKET_Sucursal>()
                .HasMany(e => e.TICKET_VentanillaAtencionSucursal)
                .WithRequired(e => e.TICKET_Sucursal)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TICKET_Ventanilla>()
                .HasMany(e => e.TICKET_VentanillaAtencionSucursal)
                .WithRequired(e => e.TICKET_Ventanilla)
                .WillCascadeOnDelete(false);
        }
    }
}
