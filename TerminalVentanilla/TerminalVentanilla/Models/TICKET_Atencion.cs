namespace TerminalVentanilla.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TICKET_Atencion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TICKET_Atencion()
        {
            TICKET_Atencion1 = new HashSet<TICKET_Atencion>();
            TICKET_VentanillaAtencionSucursal = new HashSet<TICKET_VentanillaAtencionSucursal>();
        }

        [Key]
        public int IdAtencion { get; set; }

        [Required]
        [StringLength(150)]
        public string Nombre { get; set; }

        [Required]
        [StringLength(2)]
        public string CodTipo { get; set; }

        public bool EsTercero { get; set; }

        public int? SegAtencion { get; set; }

        [Required]
        [StringLength(5)]
        public string BtnMenu { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TICKET_Atencion> TICKET_Atencion1 { get; set; }

        public virtual TICKET_Atencion TICKET_Atencion2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TICKET_VentanillaAtencionSucursal> TICKET_VentanillaAtencionSucursal { get; set; }
    }
}
