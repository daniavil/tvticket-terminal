namespace TerminalVentanilla.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TICKET_Mensaje
    {
        [Key]
        public int idMsj { get; set; }

        [Required]
        [StringLength(2000)]
        public string Mensaje { get; set; }

        public bool Habilitado { get; set; }

        public int? IdSucursal { get; set; }

        public virtual TICKET_Sucursal TICKET_Sucursal { get; set; }
    }
}
