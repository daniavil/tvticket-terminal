namespace TerminalVentanilla.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SCE_Tipo_sugerencia
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SC_Id_Tipo_Sugerencia { get; set; }

        [StringLength(50)]
        public string SC_descripcion { get; set; }
    }
}
