namespace TerminalVentanilla.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SCE_Logs
    {
        [Key]
        public int SCE_Id_evento { get; set; }

        [Required]
        [StringLength(150)]
        public string Accion { get; set; }

        [Required]
        [StringLength(256)]
        public string Mensaje { get; set; }

        public DateTime? FechaIngreso { get; set; }
    }
}
