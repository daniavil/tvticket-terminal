namespace TerminalVentanilla.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TICKET_Ticket
    {
        public Guid IdTicket { get; set; }

        [Key]
        [Column(Order = 0)]
        [StringLength(15)]
        public string CodTicket { get; set; }

        public int IdAtencion { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdSucursal { get; set; }

        public int Sucursal { get; set; }

        public int? IdVentanilla { get; set; }

        public int? Ventanilla { get; set; }

        public DateTime FechaHora { get; set; }

        public bool EnCola { get; set; }

        public bool Llamando { get; set; }

        public DateTime? FLlamado { get; set; }

        public bool Finalizo { get; set; }

        public DateTime? FechaHoraFin { get; set; }

        public bool? Atendio { get; set; }

        public DateTime? FAtendido { get; set; }

        public long? CodGestionSoeeh { get; set; }

        public bool? esPrioridad { get; set; }
    }
}
