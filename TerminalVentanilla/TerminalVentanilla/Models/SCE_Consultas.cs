namespace TerminalVentanilla.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SCE_Consultas
    {
        [Key]
        public int SCE_Id_Consulta { get; set; }

        public DateTime Fecha { get; set; }

        [StringLength(10)]
        public string Clave { get; set; }

        [StringLength(14)]
        public string Identidad { get; set; }

        [StringLength(8)]
        public string Telefono { get; set; }

        [Required]
        [StringLength(10)]
        public string SCE_Id_TipoConsulta { get; set; }

        [Required]
        [StringLength(15)]
        public string SCE_Id_Quiosko { get; set; }

        [StringLength(255)]
        public string observaciones { get; set; }

        [StringLength(255)]
        public string resultado { get; set; }

        public virtual SCE_TipoConsulta SCE_TipoConsulta { get; set; }

        public virtual SCE_Quiosko SCE_Quiosko { get; set; }
    }
}
