namespace TerminalVentanilla.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TICKET_Sucursal
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TICKET_Sucursal()
        {
            TICKET_Mensaje = new HashSet<TICKET_Mensaje>();
            TICKET_VentanillaAtencionSucursal = new HashSet<TICKET_VentanillaAtencionSucursal>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdSucursal { get; set; }

        [Required]
        [StringLength(50)]
        public string Nombre { get; set; }

        public int NumSucursal { get; set; }

        [StringLength(15)]
        public string IP { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TICKET_Mensaje> TICKET_Mensaje { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TICKET_VentanillaAtencionSucursal> TICKET_VentanillaAtencionSucursal { get; set; }
    }
}
