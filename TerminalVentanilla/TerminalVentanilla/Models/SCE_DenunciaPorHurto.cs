namespace TerminalVentanilla.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SCE_DenunciaPorHurto
    {
        [Key]
        public int DHC_Id_Denuncia { get; set; }

        [Required]
        [StringLength(10)]
        public string clave { get; set; }

        [Required]
        [StringLength(200)]
        public string Comentario { get; set; }
    }
}
