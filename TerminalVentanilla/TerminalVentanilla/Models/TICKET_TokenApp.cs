namespace TerminalVentanilla.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TICKET_TokenApp
    {
        [Key]
        public long idRow { get; set; }

        [Required]
        [StringLength(50)]
        public string token { get; set; }

        public DateTime fHoraArriboPrevisto { get; set; }

        public bool arribo { get; set; }

        public DateTime? fechaHoraArribo { get; set; }
    }
}
