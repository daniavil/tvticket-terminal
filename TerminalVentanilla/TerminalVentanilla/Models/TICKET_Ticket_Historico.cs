namespace TerminalVentanilla.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TICKET_Ticket_Historico
    {
        [Key]
        public Guid Id_Historico { get; set; }

        public int IdAtencion { get; set; }

        public int IdSucursal { get; set; }

        public int? IdVentanilla { get; set; }

        public DateTime FechaHora { get; set; }

        public bool Atendido { get; set; }

        [StringLength(15)]
        public string TiempoEspera { get; set; }

        [StringLength(15)]
        public string TiempoAtencion { get; set; }

        public bool? esPrioridad { get; set; }
    }
}
