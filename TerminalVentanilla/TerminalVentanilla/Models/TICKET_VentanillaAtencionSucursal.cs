namespace TerminalVentanilla.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TICKET_VentanillaAtencionSucursal
    {
        [Key]
        public int IdVenAteSuc { get; set; }

        public int IdSucursal { get; set; }

        public int IdVentanilla { get; set; }

        public int IdAtencion { get; set; }

        public bool Atendiendo { get; set; }

        public bool Habilitado { get; set; }

        public int? SigNum { get; set; }

        public virtual TICKET_Atencion TICKET_Atencion { get; set; }

        public virtual TICKET_Sucursal TICKET_Sucursal { get; set; }

        public virtual TICKET_Ventanilla TICKET_Ventanilla { get; set; }
    }
}
