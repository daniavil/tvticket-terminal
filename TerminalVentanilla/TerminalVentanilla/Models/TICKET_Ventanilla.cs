namespace TerminalVentanilla.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TICKET_Ventanilla
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TICKET_Ventanilla()
        {
            TICKET_VentanillaAtencionSucursal = new HashSet<TICKET_VentanillaAtencionSucursal>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdVentanilla { get; set; }

        [Required]
        [StringLength(50)]
        public string Ventanilla { get; set; }

        public int NumVentanilla { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TICKET_VentanillaAtencionSucursal> TICKET_VentanillaAtencionSucursal { get; set; }
    }
}
