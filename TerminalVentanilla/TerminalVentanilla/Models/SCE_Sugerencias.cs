namespace TerminalVentanilla.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class SCE_Sugerencias
    {
        [Key]
        public int SC_Id_Sugerencia { get; set; }

        [Required]
        [StringLength(200)]
        public string Descripcion { get; set; }

        public int? SC_Id_Tipo_Sugerencia { get; set; }
    }
}
