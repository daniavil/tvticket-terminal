﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;
using TerminalVentanilla.Controllers;

namespace TerminalVentanilla
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            if (ControlVersion())
            {
                if (PriorProcess() != null)
                {

                    MessageBox.Show("Actualmente ya hay en ejecución una instancia de este Programa.");
                    return;
                }
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Form1());
            }
            else
            {
                if (MessageBox.Show("ATENCIÓN.! EXISTE UNA VERSIÓN MAS RECIENTE DE ESTE SISTEMA. POR FAVOR CONTACTAR A TIC PARA ACTUALIZAR ESTE SISTEMA",
                    "INFORMACIÓN", MessageBoxButtons.OK) == DialogResult.OK)
                {
                    Process.GetCurrentProcess().Kill();
                }
            }            
        }

        public static Process PriorProcess()
        {
            Process curr = Process.GetCurrentProcess();
            Process[] procs = Process.GetProcessesByName(curr.ProcessName);
            foreach (Process p in procs)
            {
                if ((p.Id != curr.Id) &&
                    (p.MainModule.FileName == curr.MainModule.FileName))
                    return p;
            }
            return null;
        }

        static bool ControlVersion()
        {
            string IdSys = ConfigurationManager.AppSettings["IdSys"];
            string AppName = Assembly.GetEntryAssembly().GetName().Name;
            try
            {
                bool abrir = false;
                string conn = ConfigurationManager.ConnectionStrings["SysConn"].ConnectionString;
                using (SqlConnection sqlConnection = new SqlConnection(conn))
                {
                    string qry = $@"select case COUNT(*) when 0 then 'False' else 'True' end AS VERCORRECTA, IdVersion
                    from SysControlVersion
                    where IdSistema='{IdSys}' and NomEnsamblado='{AppName}'
                    AND IdVersion=(select MAX(IdVersion) from SysControlVersion where NomEnsamblado='{AppName}')
                    group by IdVersion";
                    SqlCommand sqlCmd = new SqlCommand(qry, sqlConnection);
                    sqlConnection.Open();
                    SqlDataReader sqlReader = sqlCmd.ExecuteReader();
                    while (sqlReader.Read())
                    {
                        abrir = Convert.ToBoolean(sqlReader["VERCORRECTA"].ToString());
                        ConfigClass.idVersion = sqlReader["IdVersion"].ToString();
                    }
                    sqlReader.Close();
                }
                return abrir;
            }
            catch (Exception EX)
            {
                MessageBox.Show($"SE PRODUJO UNA EXCEPCIÓN: {EX.Message}");
                return false;
            }
        }

    }
}
