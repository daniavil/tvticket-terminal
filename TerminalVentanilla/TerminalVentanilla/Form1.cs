﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TerminalVentanilla.Controllers;

namespace TerminalVentanilla
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                CargarGridVentanillas();
                ShowInTaskbar = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ha ocurrido una excepción: {ex.Message}");
                Process.GetCurrentProcess().Kill();
            }
            
        }

        private void CargarGridVentanillas()
        {
            dgVentanillas.DataSource = DataVentanilla.GetVentanillas();
        }

        private void dgVentanillas_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (IsANonHeaderButtonCell(e))
            {
                if (MessageBox.Show("¿Está seguro(a) de tomar esta ventanilla para operar?", "ATENCIÓN", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    if (DataVentanilla.VerificaVentanillas(int.Parse(dgVentanillas[1, e.RowIndex].Value.ToString())))
                    {
                        MessageBox.Show(
                            "No se puede tomar esta ventanilla, ya previamente ha sido tomada por otra persona.",
                            "ERROR");
                    }
                    else
                    {
                        Global.NumVentanilla = int.Parse(dgVentanillas[1, e.RowIndex].Value.ToString());
                        Global.IdAtencion = int.Parse(dgVentanillas[3, e.RowIndex].Value.ToString());
                        Global.IdVentanilla = int.Parse(dgVentanillas[4, e.RowIndex].Value.ToString());
                        FrmVentanilla frmVentanilla = new FrmVentanilla();
                        frmVentanilla.Owner = this;
                        frmVentanilla.Show();
                        Hide();
                    }
                }
            }
        }

        private bool IsANonHeaderButtonCell(DataGridViewCellEventArgs cellEvent)
        {
            if (dgVentanillas.Columns[cellEvent.ColumnIndex] is
                DataGridViewButtonColumn &&
                cellEvent.RowIndex != -1)
            { return true; }
            else { return (false); }
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == WindowState)
            {
                Hide();
                trayIcon.Visible = true;
            }
        }

        private void trayIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Show();
            WindowState = FormWindowState.Normal;
            trayIcon.Visible = false;
        }

        private void mnuSalir_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("¿Desea Salir del Programa?", "VENTANILLA TURNO EEH", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                Process.GetCurrentProcess().Kill();
            }
        }

        private void mnuhabiliarVentanilla_Click(object sender, EventArgs e)
        {
            Form2 form2=new Form2();
            form2.Owner = this;
            form2.ShowDialog();
        }
    }
}
