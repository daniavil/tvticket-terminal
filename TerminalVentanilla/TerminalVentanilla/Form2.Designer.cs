﻿namespace TerminalVentanilla
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtNumVentanilla = new System.Windows.Forms.NumericUpDown();
            this.btnLiberar = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumVentanilla)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnLiberar);
            this.panel1.Controls.Add(this.txtNumVentanilla);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(274, 61);
            this.panel1.TabIndex = 0;
            // 
            // txtNumVentanilla
            // 
            this.txtNumVentanilla.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.txtNumVentanilla.Location = new System.Drawing.Point(39, 23);
            this.txtNumVentanilla.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.txtNumVentanilla.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtNumVentanilla.Name = "txtNumVentanilla";
            this.txtNumVentanilla.Size = new System.Drawing.Size(62, 20);
            this.txtNumVentanilla.TabIndex = 0;
            this.txtNumVentanilla.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtNumVentanilla.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // btnLiberar
            // 
            this.btnLiberar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnLiberar.BackColor = System.Drawing.Color.LightGreen;
            this.btnLiberar.Location = new System.Drawing.Point(107, 14);
            this.btnLiberar.Name = "btnLiberar";
            this.btnLiberar.Size = new System.Drawing.Size(126, 35);
            this.btnLiberar.TabIndex = 1;
            this.btnLiberar.Text = "Liberar Ventanilla";
            this.btnLiberar.UseVisualStyleBackColor = false;
            this.btnLiberar.Click += new System.EventHandler(this.btnLiberar_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(274, 61);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(290, 100);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(290, 100);
            this.Name = "Form2";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ventanillas Tomadas";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNumVentanilla)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.NumericUpDown txtNumVentanilla;
        private System.Windows.Forms.Button btnLiberar;
    }
}