﻿namespace TerminalVentanilla
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgVentanillas = new System.Windows.Forms.DataGridView();
            this.Dato1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Dato2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Dato3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Dato4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col3 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.trayIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuhabiliarVentanilla = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSalir = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgVentanillas)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(675, 60);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 35F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(155, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(375, 54);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tomar Ventanilla";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgVentanillas);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 84);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(675, 241);
            this.panel2.TabIndex = 1;
            // 
            // dgVentanillas
            // 
            this.dgVentanillas.AllowUserToAddRows = false;
            this.dgVentanillas.AllowUserToDeleteRows = false;
            this.dgVentanillas.AllowUserToOrderColumns = true;
            this.dgVentanillas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgVentanillas.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgVentanillas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgVentanillas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Dato1,
            this.Dato2,
            this.Dato3,
            this.Dato4,
            this.col3});
            this.dgVentanillas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgVentanillas.Location = new System.Drawing.Point(0, 0);
            this.dgVentanillas.MultiSelect = false;
            this.dgVentanillas.Name = "dgVentanillas";
            this.dgVentanillas.ReadOnly = true;
            this.dgVentanillas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgVentanillas.Size = new System.Drawing.Size(675, 241);
            this.dgVentanillas.TabIndex = 0;
            this.dgVentanillas.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgVentanillas_CellContentClick);
            // 
            // Dato1
            // 
            this.Dato1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Dato1.DataPropertyName = "Dato1";
            this.Dato1.HeaderText = "N° Ventanilla";
            this.Dato1.Name = "Dato1";
            this.Dato1.ReadOnly = true;
            this.Dato1.Width = 86;
            // 
            // Dato2
            // 
            this.Dato2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Dato2.DataPropertyName = "Dato2";
            this.Dato2.HeaderText = "Atención";
            this.Dato2.Name = "Dato2";
            this.Dato2.ReadOnly = true;
            // 
            // Dato3
            // 
            this.Dato3.DataPropertyName = "Dato3";
            this.Dato3.HeaderText = "Dato3";
            this.Dato3.Name = "Dato3";
            this.Dato3.ReadOnly = true;
            this.Dato3.Visible = false;
            this.Dato3.Width = 61;
            // 
            // Dato4
            // 
            this.Dato4.DataPropertyName = "Dato4";
            this.Dato4.HeaderText = "Dato4";
            this.Dato4.Name = "Dato4";
            this.Dato4.ReadOnly = true;
            this.Dato4.Visible = false;
            this.Dato4.Width = 61;
            // 
            // col3
            // 
            this.col3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.col3.HeaderText = "Tomar Ventanilla";
            this.col3.Name = "col3";
            this.col3.ReadOnly = true;
            this.col3.Text = "Tomar Ventanilla";
            this.col3.UseColumnTextForButtonValue = true;
            this.col3.Width = 83;
            // 
            // trayIcon
            // 
            this.trayIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("trayIcon.Icon")));
            this.trayIcon.Text = "Cliente Ticket";
            this.trayIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.trayIcon_MouseDoubleClick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(675, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuhabiliarVentanilla,
            this.mnuSalir});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.archivoToolStripMenuItem.Text = "Archivo";
            // 
            // mnuhabiliarVentanilla
            // 
            this.mnuhabiliarVentanilla.Name = "mnuhabiliarVentanilla";
            this.mnuhabiliarVentanilla.Size = new System.Drawing.Size(180, 22);
            this.mnuhabiliarVentanilla.Text = "Habilitar Ventanilla";
            this.mnuhabiliarVentanilla.Click += new System.EventHandler(this.mnuhabiliarVentanilla_Click);
            // 
            // mnuSalir
            // 
            this.mnuSalir.Name = "mnuSalir";
            this.mnuSalir.Size = new System.Drawing.Size(180, 22);
            this.mnuSalir.Text = "Salir";
            this.mnuSalir.Click += new System.EventHandler(this.mnuSalir_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(675, 325);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Selección de Ventanilla";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgVentanillas)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dgVentanillas;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Dato1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Dato2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Dato3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Dato4;
        private System.Windows.Forms.DataGridViewButtonColumn col3;
        private System.Windows.Forms.NotifyIcon trayIcon;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnuhabiliarVentanilla;
        private System.Windows.Forms.ToolStripMenuItem mnuSalir;
    }
}

