﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Timers;
using System.Windows.Forms;
using TerminalVentanilla.Controllers;
using TerminalVentanilla.Models;
using Timer = System.Timers.Timer;

namespace TerminalVentanilla
{
    public partial class FrmVentanilla : Form
    {
        public static Timer TimerCliente;
        public static Timer TimerAtencion;
        public bool Atendiendo = false;
        Stopwatch watch = new Stopwatch();

        private void SetPositionForm()
        {
            this.StartPosition = FormStartPosition.Manual;
            this.Location = new Point(Screen.PrimaryScreen.WorkingArea.Width - this.Width, Screen.PrimaryScreen.WorkingArea.Height - this.Height);
        }

        public FrmVentanilla()
        {
            InitializeComponent();
            SetPositionForm();
            ListadoVentanillas();
        }

        public void ListadoVentanillas()
        {
            chkVentanillas.DataSource = DataAtencion.GetListaAtencionesSucursalDto(false);
            chkVentanillas.DisplayMember = "Dato2";
            chkVentanillas.ValueMember = "Dato1";
        }

        private void FrmVentanilla_Load(object sender, EventArgs e)
        {
            DataVentanilla.TomarVentanillaSucursal();
            dgClientes.AutoGenerateColumns = false;
            dgPrioridad.AutoGenerateColumns = false;
            TimerCliente = new Timer();
            TimerCliente.Interval = 1000;
            TimerCliente.Enabled = true;
            TimerCliente.AutoReset = false;
            TimerCliente.Elapsed += OnTimedEvent_TimerCliente;
            TimerCliente.Start();
            Text = "(v" + ConfigClass.idVersion +  ") - Ventanilla - " + Global.NumVentanilla;
            BuscarCliente();

            TimerAtencion = new Timer();
            TimerAtencion.Interval = 1000;
            TimerAtencion.Enabled = true;
            TimerAtencion.AutoReset = false;
            TimerAtencion.Elapsed += OnTimedEvent_TimerAtencion;
            TimerAtencion.Start();

            List<ObjClass> atenciones=new List<ObjClass>();
            atenciones= DataAtencion.GetListaAtencionesDto();
            cboAtencion.DataSource = atenciones;
            cboAtencion.DisplayMember = "Dato2";
            cboAtencion.ValueMember = "Dato1";
            cboAtencion.Width = setWidth_comboBox(cboAtencion);
            cboAtencion.SelectedIndex = -1;
            //ShowInTaskbar = false;

            dgPrioridad.Visible = !DataAtencion.AtencionEsPrioridad(Global.IdAtencion);

            //


        }

        static int setWidth_comboBox(ComboBox cb)
        {
            int maxWidth = 0, temp = 0;
            foreach (ObjClass s in cb.Items)
            {
                temp = TextRenderer.MeasureText(s.Dato2, cb.Font).Width;
                if (temp > maxWidth)
                {
                    maxWidth = temp;
                }
            }
            return maxWidth + SystemInformation.VerticalScrollBarWidth;
        }

        private void OnTimedEvent_TimerCliente(object source, ElapsedEventArgs e)
        {
            BeginInvoke((Action)(() => //Invoke at UI thread
            {
                TimerCliente.Enabled = true;
                BuscarCliente();
                LlenarGridCliente();
            }), null);
        }

        private void OnTimedEvent_TimerAtencion(object source, ElapsedEventArgs e)
        {
            BeginInvoke((Action)(() =>
            {
                TimerAtencion.Enabled = true;
                lblTiempoTranscurrido.Text = "Duración: " + watch.Elapsed.Minutes + ":" + watch.Elapsed.Seconds + " min";
               // TimerAtencion.Start();
            }), null);
        }

        private void FrmVentanilla_LocationChanged(object sender, EventArgs e)
        {
            SetPositionForm();
        }

        private void BuscarCliente()
        {
            TICKET_Ticket ticket=new TICKET_Ticket();           
            ticket = DataCliente.GetClienteVentanilla();
            if (ticket != null)
            {
                txtTicket.Text = ticket.CodTicket;
                btnAtendiendo.Enabled = true;
                dgClientes.Enabled = false;
                dgPrioridad.Enabled = false;
                btnFinalizar.Enabled = true;
                cboAtencion.Enabled = true;
            }
            else
            {
                txtTicket.Text = "";
                btnAtendiendo.Enabled = false;
                dgClientes.Enabled = true;
                dgPrioridad.Enabled = true;
                btnFinalizar.Enabled = false;
                cboAtencion.Enabled = false;
            }
        }

        private void LlenarGridCliente()
        {
            List<int?> listaVentanillas = new List<int?>();
            for (int i = 0; i < chkVentanillas.Items.Count; i++)
            {
                if (chkVentanillas.GetItemChecked(i))
                    listaVentanillas.Add(Convert.ToInt32(((ObjClass)chkVentanillas.Items[i]).Dato1));
            }
            listaVentanillas.Add(Global.IdAtencion);
            dgClientes.AutoGenerateColumns = false;
            dgClientes.DataSource = DataCliente.GetListaClientesEnCola(listaVentanillas);

            var listaPrio = DataCliente.GetListaClientesEnColaPrioridad();
            dgPrioridad.AutoGenerateColumns = false;
            dgPrioridad.DataSource = listaPrio;
            dgPrioridad.Visible = listaPrio.Count > 0 ? true : false;
            //dgPrioridad.Visible = !DataAtencion.AtencionEsPrioridad(Global.IdAtencion);

        }

        private void btnAtendiendo_Click(object sender, EventArgs e)
        {
            if (txtTicket.Text != "")
            {
                if (MessageBox.Show("¿Llegó cliente?", "ATENDER CLIENTE", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    DataCliente.AtenderCliente(txtTicket.Text.Trim());
                    TimerCliente.Stop();
                    WindowState = FormWindowState.Minimized;
                    btnAtendiendo.Enabled = false;
                    dgClientes.Enabled = false;
                    dgPrioridad.Enabled = false;
                    btnFinalizar.Enabled = true;
                    cboAtencion.Enabled = true;
                    Atendiendo = true;
                    watch.Restart();
                    TimerAtencion.Start();
                    watch.Start();
                                       
                }
            }
        }

        private void btnFinalizar_Click(object sender, EventArgs e)
        {
            if (txtTicket.Text.Trim() != "" && !Atendiendo)
            {
                if (MessageBox.Show("¿Desear Finalizar la atención Porque Cliente no Llegó?", "FINALIZAR ATENCIÓN", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    DataCliente.FinalizarClienteSinAtencion(txtTicket.Text.Trim());
                    TimerCliente.Start();
                    dgClientes.Enabled = false;
                    dgPrioridad.Enabled = false;
                    Atendiendo = false;
                }
            }
            else if (txtTicket.Text.Trim() != "")
            {
                if (MessageBox.Show("¿Desea finalizar la atención del cliente?", "FINALIZAR ATENCIÓN", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    DataCliente.FinalizarCliente(txtTicket.Text.Trim(), (long)txtGestSoeeh.Value);
                    TimerCliente.Start();
                    dgClientes.Enabled = false;
                    dgPrioridad.Enabled = false;
                    Atendiendo = false;
                    watch.Stop();
                    watch.Restart();
                    TimerAtencion.Stop();
                    TimerAtencion.Enabled = false;
                    TimerAtencion.AutoReset = true;
                    lblTiempoTranscurrido.Text = "";
                    txtGestSoeeh.Value = 0;
                }
            }
        }

        private void dgClientes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (IsANonHeaderButtonCelldgClientes(e))
            {
                string ticket = dgClientes[0, e.RowIndex].Value.ToString();
                DataCliente.SetClienteVentanilla(ticket);
            }
        }
        
        private void dgPrioridad_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (IsANonHeaderButtonCelldgPrioridad(e))
            {
                string ticket = dgPrioridad[0, e.RowIndex].Value.ToString();
                DataCliente.SetClienteVentanilla(ticket);
            }
        }

        private bool IsANonHeaderButtonCelldgClientes(DataGridViewCellEventArgs cellEvent)
        {
            if (dgPrioridad.Columns[cellEvent.ColumnIndex] is
                DataGridViewButtonColumn &&
                cellEvent.RowIndex != -1)
            { return true; }
            else { return (false); }
        }

        private bool IsANonHeaderButtonCelldgPrioridad(DataGridViewCellEventArgs cellEvent)
        {
            if (dgClientes.Columns[cellEvent.ColumnIndex] is
                DataGridViewButtonColumn &&
                cellEvent.RowIndex != -1)
            { return true; }
            else { return (false); }
        }

        private void cboAtencion_SelectionChangeCommitted(object sender, EventArgs e)
        {
            ObjClass obj=new ObjClass();
            obj = (ObjClass) cboAtencion.SelectedItem;
            if (MessageBox.Show("¿Desea Cambiar Cliente a: "+ Environment.NewLine + obj.Dato2 + " ?",
                "CAMBIAR VENTANILLA/ATENCIÓN", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                DataCliente.CambiarClienteAtencion(int.Parse(obj.Dato1), txtTicket.Text.Trim());
                TimerCliente.Start();
                dgClientes.Enabled = false;
                dgPrioridad.Enabled = false;

                watch.Stop();
                watch.Restart();
                TimerAtencion.Stop();
                TimerAtencion.Enabled = false;
                TimerAtencion.AutoReset = true;
                lblTiempoTranscurrido.Text = "";
            }
            cboAtencion.SelectedIndex = -1;
        }

        private void FrmVentanilla_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("¿Está seguro(a) de cerrar este formulario?",
                "ATENCIÓN", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                DataVentanilla.CerrarVentanillaSucursal();
                TimerCliente.Stop();
                TimerAtencion.Stop();
                Global.Inicializar();
                Owner.Show();
            }
            else
            {
                e.Cancel = true;
            }
        }
    }
}
