﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalVentanilla.Models;

namespace TerminalVentanilla.Controllers
{
    public static class DataVentanilla
    {
        public static List<ObjClass> GetVentanillas()
        {
            using (var db = new TicketModel())
            {
                var qry = (from tb in db.TICKET_VentanillaAtencionSucursal
                    where tb.TICKET_Sucursal.IdSucursal == Global.IdSucursal && tb.Habilitado
                           select new ObjClass
                           {
                               Dato1 = tb.TICKET_Ventanilla.NumVentanilla.ToString(),
                               Dato2 = tb.TICKET_Atencion.Nombre,
                               Dato3 = tb.IdAtencion.ToString(),
                               Dato4 = tb.IdVentanilla.ToString()
                           }).ToList();
                return qry;
            }
        }

        public static bool VerificaVentanillas(int numVentanillaTomar)
        {
            using (var db = new TicketModel())
            {
                var qry = (from tb in db.TICKET_VentanillaAtencionSucursal
                    join v in db.TICKET_Ventanilla on tb.IdVentanilla equals v.IdVentanilla
                    where tb.TICKET_Sucursal.IdSucursal == Global.IdSucursal && v.NumVentanilla == numVentanillaTomar && tb.Habilitado
                           select tb.Atendiendo).FirstOrDefault();
                return qry;
            }
        }

        public static void TomarVentanillaSucursal()
        {
            using (var db = new TicketModel())
            {
                var qry = (from tb in db.TICKET_VentanillaAtencionSucursal
                           join v in db.TICKET_Ventanilla on tb.IdVentanilla equals v.IdVentanilla
                           join s in db.TICKET_Sucursal on tb.IdSucursal equals s.IdSucursal
                           where v.NumVentanilla == Global.NumVentanilla 
                           && tb.IdAtencion == Global.IdAtencion 
                           && s.IdSucursal == Global.IdSucursal
                           select tb).FirstOrDefault();

                qry.Atendiendo = true;
                db.Entry(qry).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public static void CerrarVentanillaSucursal()
        {
            using (var db = new TicketModel())
            {
                var qry = (from tb in db.TICKET_VentanillaAtencionSucursal
                           join v in db.TICKET_Ventanilla on tb.IdVentanilla equals v.IdVentanilla
                    join s in db.TICKET_Sucursal on tb.IdSucursal equals s.IdSucursal
                           where v.NumVentanilla==Global.NumVentanilla && tb.IdAtencion==Global.IdAtencion && s.IdSucursal==Global.IdSucursal
                           select tb).FirstOrDefault();

                qry.Atendiendo = false;
                db.Entry(qry).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public static int LiberarVentanillaSucursal(int numVentanilla)
        {
            using (var db = new TicketModel())
            {
                var qry = (from tb in db.TICKET_VentanillaAtencionSucursal
                           join v in db.TICKET_Ventanilla on tb.IdVentanilla equals v.IdVentanilla
                           join s in db.TICKET_Sucursal on tb.IdSucursal equals s.IdSucursal
                           where v.NumVentanilla == numVentanilla && s.IdSucursal == Global.IdSucursal
                           select tb).FirstOrDefault();

                qry.Atendiendo = false;
                db.Entry(qry).State = EntityState.Modified;
                return db.SaveChanges();
            }
        }
    }
}
