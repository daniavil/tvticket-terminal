﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalVentanilla.Models;

namespace TerminalVentanilla.Controllers
{
    public static class DataCliente
    {
        public static TICKET_Ticket GetClienteVentanilla()
        {
            using (var db = new TicketModel())
            {
                var qry = (from tb in db.TICKET_Ticket
                    where tb.IdSucursal == Global.IdSucursal && tb.Ventanilla==Global.NumVentanilla  && !tb.Finalizo
                    select tb).FirstOrDefault();
                return qry;
            }
        }

        public static List<TICKET_Ticket> GetListaClientesEnCola(List<int?> lista)
        {
            TicketModel db = new TicketModel();
            var qry = db.TICKET_Ticket
                .Where(t => t.EnCola
                && !t.Finalizo
                && t.IdSucursal == Global.IdSucursal
                && lista.Contains(t.IdAtencion)
                && t.IdVentanilla == null
                && t.Ventanilla == null)
                .OrderBy(t => t.FechaHora).ToList();
            return qry;
        }

        public static List<TICKET_Ticket> GetListaClientesEnColaPrioridad()
        {
            TicketModel db = new TicketModel();
            var qry = db.TICKET_Ticket
                .Where(t => t.EnCola
                && !t.Finalizo
                && t.IdSucursal == Global.IdSucursal
                && t.IdVentanilla == null
                && t.Ventanilla == null
                && t.esPrioridad == true)
                .OrderBy(t => t.FechaHora).ToList();
            return qry;
        }

        public static void AtenderCliente(string codticket)
        {
            using (var db = new TicketModel())
            {
                var qry = (from tb in db.TICKET_Ticket
                           where tb.IdSucursal == Global.IdSucursal && tb.Ventanilla == Global.NumVentanilla
                           && !tb.Finalizo && tb.CodTicket.Equals(codticket)
                           select tb).FirstOrDefault();
                qry.EnCola = false;
                qry.Llamando = false;
                qry.FAtendido = DateTime.Now;
                db.Entry(qry).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public static void SetClienteVentanilla(string codticket)
        {
            using (var db = new TicketModel())
            {
                var qry = (from tb in db.TICKET_Ticket
                           where tb.IdSucursal == Global.IdSucursal && tb.CodTicket.Equals(codticket)
                           select tb).FirstOrDefault();

                bool hayUnoAtendiendo = (from tb in db.TICKET_Ticket 
                                         where tb.Llamando 
                                         && tb.IdSucursal == Global.IdSucursal 
                                         select tb).Any();

                qry.EnCola = true;
                qry.IdVentanilla = Global.IdVentanilla;
                qry.Ventanilla = Global.NumVentanilla;
                qry.FLlamado = DateTime.Now;
                qry.Llamando = !hayUnoAtendiendo;
                db.Entry(qry).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public static void FinalizarCliente(string codticket, long? CodGestionSoeeh)
        {
            using (var db = new TicketModel())
            {
                var qry = (from tb in db.TICKET_Ticket
                           where tb.IdSucursal== Global.IdSucursal && tb.Ventanilla == Global.NumVentanilla
                           && !tb.Finalizo && tb.CodTicket.Equals(codticket)
                           select tb).FirstOrDefault();
                qry.EnCola = false;
                qry.Llamando = false;
                qry.Finalizo = true;
                qry.Atendio = true;
                qry.FechaHoraFin=DateTime.Now;
                qry.CodGestionSoeeh = CodGestionSoeeh;
                db.Entry(qry).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public static void FinalizarClienteSinAtencion(string codticket)
        {
            using (var db = new TicketModel())
            {
                var qry = (from tb in db.TICKET_Ticket
                           where tb.IdSucursal == Global.IdSucursal && tb.Ventanilla == Global.NumVentanilla
                           && !tb.Finalizo && tb.CodTicket.Equals(codticket)
                           select tb).FirstOrDefault();
                qry.EnCola = false;
                qry.Llamando = false;
                qry.Finalizo = true;
                qry.Atendio = false;
                qry.FechaHoraFin = DateTime.Now;
                qry.CodGestionSoeeh = 0;
                db.Entry(qry).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public static void CambiarClienteAtencion(int idAtencion, string codticket)
        {
            using (var db = new TicketModel())
            {
                var qry = (from tb in db.TICKET_Ticket
                           where tb.IdSucursal == Global.IdSucursal && tb.CodTicket.Equals(codticket)
                           select tb).FirstOrDefault();

                //bool hayUnoAtendiendo = (from tb in db.TICKET_Ticket where tb.Llamando && tb.IdSucursal == Global.IdSucursal select tb).Any();

                qry.EnCola = true;
                qry.IdVentanilla = null;
                qry.Ventanilla = null;
                qry.Llamando = false; //!hayUnoAtendiendo;
                qry.IdAtencion = idAtencion;
                db.Entry(qry).State = EntityState.Modified;
                db.SaveChanges();
            }
        }
    }
}
