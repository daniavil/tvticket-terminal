﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerminalVentanilla.Models;

namespace TerminalVentanilla.Controllers
{
    public static class DataAtencion
    {
        public static List<ObjClass> GetListaAtencionesDto()
        {
            using (var db = new TicketModel())
            {
                var qry = (from tb in db.TICKET_VentanillaAtencionSucursal
                           where tb.TICKET_Ventanilla.IdVentanilla!=Global.IdVentanilla &&
                           tb.IdAtencion!=Global.IdAtencion && tb.Habilitado && tb.TICKET_Sucursal.IdSucursal ==Global.IdSucursal
                           select new ObjClass
                           {
                               Dato1 = tb.TICKET_Atencion.IdAtencion.ToString(),
                               Dato2 = "V-"+tb.TICKET_Ventanilla.NumVentanilla+" / "+tb.TICKET_Atencion.Nombre
                           }).ToList();
                return qry;
            }
        }

        public static List<ObjClass> GetListaAtencionesSucursalDto(bool esPrio)
        {
            using (var db = new TicketModel())
            {
                var qry = (from a in db.TICKET_Atencion
                           join tb in db.TICKET_VentanillaAtencionSucursal on a.IdAtencion equals tb.IdAtencion
                           where tb.TICKET_Ventanilla.IdVentanilla != Global.IdVentanilla &&
                           tb.IdAtencion != Global.IdAtencion && tb.Habilitado && tb.TICKET_Sucursal.IdSucursal == Global.IdSucursal
                           && a.EsTercero == esPrio
                           select new ObjClass
                           {
                               Dato1 = a.IdAtencion.ToString(),
                               Dato2 = a.Nombre//"V-" + tb.TICKET_Ventanilla.NumVentanilla + " / " + tb.TICKET_Atencion.Nombre
                           }).Distinct().ToList();
                return qry;
            }
        }

        public static bool AtencionEsPrioridad(int idAtencion)
        {
            using (var db = new TicketModel())
            {
                var atencion = db.TICKET_Atencion.Find(idAtencion);
                return atencion.EsTercero;
            }
        }
    }
}
