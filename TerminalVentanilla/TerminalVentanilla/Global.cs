﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TerminalVentanilla
{
    public static class Global
    {
        public static int NumVentanilla;
        public static int IdVentanilla;
        public static int IdAtencion;
        public static readonly int IdSucursal= int.Parse(System.Configuration.ConfigurationManager.AppSettings["IdSucursal"]);

        public static void Inicializar()
        {
            NumVentanilla = 0;
            IdVentanilla = 0;
        }
    }
}
