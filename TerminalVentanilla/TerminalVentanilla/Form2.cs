﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TerminalVentanilla.Controllers;

namespace TerminalVentanilla
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void btnLiberar_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("¿Esta Seguro que desea liberar la ventanilla solicitada?", "VENTANILLA", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                try
                {
                    if (DataVentanilla.LiberarVentanillaSucursal(int.Parse(txtNumVentanilla.Text)) > 0)
                    {
                        MessageBox.Show("Se libero la ventanilla");
                    }
                    else
                    {
                        MessageBox.Show("Se presento un problema al liberar la ventanilla");
                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show("Excepción: "+ ex.Message);
                }
            }
        }
    }
}
